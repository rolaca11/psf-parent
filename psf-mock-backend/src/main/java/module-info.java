open module psf.mock.backend {
    requires psf.api;
    requires psf.camera.api;
    requires spring.data.jpa;
    requires java.persistence;
    requires lombok;
    requires jakarta.enterprise.cdi.api;
    requires jakarta.ejb.api;
    requires jakarta.inject.api;
    requires org.mapstruct;
    requires java.ws.rs;

    requires java.sql;
    requires java.annotation;
    requires java.compiler;
}
