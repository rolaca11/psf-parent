package io.ropi.psf.backend.mock.plugin;

import javax.ejb.Stateless;
import java.util.List;

import io.ropi.psf.api.parkingspace.ParkingSpaceDto;
import io.ropi.psf.api.plugin.DataSource;

@Stateless
public class MockDataSource implements DataSource {

    @Override
    public List<ParkingSpaceDto> getParkingSpaces(Long parkingLotId) {
        return null;
    }
}
