import { Component, OnInit } from '@angular/core';
import {Test} from "../services/test/test";

@Component({
  selector: 'app-test-component',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  public testString: string;

  constructor(private testService: Test) { }

  ngOnInit() {
    this.testService.test().subscribe(value => this.testString = value);
  }
}
