import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { testProvider } from "./services/test/test.provider";
import { HomepageComponent } from './homepage/homepage.component';
import { BsDropdownModule, CollapseModule } from "ngx-bootstrap";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ServiceViewerComponent } from './service-viewer/service-viewer.component';
import { metaDataSourceProvider } from "./services/metadatasource/metadatasource.provider";

@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    HomepageComponent,
    ServiceViewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot()
  ],
  providers: [
    testProvider,
    metaDataSourceProvider
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
