import { Injectable } from '@angular/core';
import {Test} from "./test";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TestMock extends Test {

  constructor() {
    super();
  }

  test() : Observable<string> {
    return new Observable<string>(subscriber => subscriber.next("mock"));
  }
}
