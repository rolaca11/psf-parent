import {Test} from "./test";
import {TestMock} from "./test.mock";
import { isDevMode } from "@angular/core";
import { TestService } from "./test.service";
import { HttpClient } from "@angular/common/http";

export let testProvider = { provide: Test,
  useFactory: (http: HttpClient) => isDevMode() ? new TestMock() : new TestService(http),
  deps: [HttpClient]
};


