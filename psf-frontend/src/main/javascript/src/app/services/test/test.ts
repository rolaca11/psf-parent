import {Observable} from "rxjs";

export abstract class Test {
  abstract test(): Observable<string>;
}
