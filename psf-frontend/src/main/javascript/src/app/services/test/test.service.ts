import { Injectable } from '@angular/core';
import {Test} from "./test";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TestService extends Test {

  constructor(private http: HttpClient) {
    super();
  }

  test() : Observable<string> {
    return this.http.get<string>("api/");
  }
}
