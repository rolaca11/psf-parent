import { MetaDataSource } from "./metadatasource";
import { MetaDataSourceMock } from "./metadatasource.mock";
import { isDevMode } from "@angular/core";
import { MetaDataSourceService } from "./metadatasource.service";
import { HttpClient } from "@angular/common/http";

export let metaDataSourceProvider = {
  provide: MetaDataSource,
  useFactory: (http: HttpClient) => isDevMode() ? new MetaDataSourceMock() : new MetaDataSourceService(http),
  deps: [HttpClient]
};


