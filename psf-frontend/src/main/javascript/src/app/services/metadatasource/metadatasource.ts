import { Observable } from "rxjs";

export abstract class MetaDataSource {
  abstract getMetaDataSources(): Observable<MetaDataSourceDto[]>;
}

export class MetaDataSourceDto {
  private _id: string;
  private _name: string;
  private _uptime: number;


  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get uptime(): number {
    return this._uptime;
  }

  set uptime(value: number) {
    this._uptime = value;
  }
}
