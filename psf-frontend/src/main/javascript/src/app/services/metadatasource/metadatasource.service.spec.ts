import { TestBed } from '@angular/core/testing';

import { MetaDataSourceService } from './metadatasource.service';

describe('TestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MetaDataSourceService = TestBed.get(MetaDataSourceService);
    expect(service).toBeTruthy();
  });
});
