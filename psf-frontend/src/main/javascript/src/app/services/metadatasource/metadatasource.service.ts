import { Injectable } from '@angular/core';
import { MetaDataSource, MetaDataSourceDto } from "./metadatasource";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MetaDataSourceService extends MetaDataSource {

  constructor(private http: HttpClient) {
    super();
  }

  getMetaDataSources() : Observable<MetaDataSourceDto[]> {
    return this.http.get<MetaDataSourceDto[]>("api/metadatasource");
  }
}
