import { Injectable } from '@angular/core';
import {MetaDataSource, MetaDataSourceDto} from "./metadatasource";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MetaDataSourceMock extends MetaDataSource {

  constructor() {
    super();
  }

  getMetaDataSources() : Observable<MetaDataSourceDto[]> {
    let metaDataSource : MetaDataSourceDto = new MetaDataSourceDto();
    metaDataSource.id = "mock";
    metaDataSource.name = "Mocked datasource";
    metaDataSource.uptime = 1000;

    let result: MetaDataSourceDto[] = [
      metaDataSource,
      metaDataSource,
      metaDataSource,
      metaDataSource,
      metaDataSource,
      metaDataSource,
      metaDataSource
    ];

    return new Observable<MetaDataSourceDto[]>(subscriber => subscriber.next(result));
  }
}
