import { Component, OnInit } from '@angular/core';
import { MetaDataSource, MetaDataSourceDto } from "../services/metadatasource/metadatasource";
import { Observable } from "rxjs";

@Component({
  selector: 'app-service-viewer',
  templateUrl: './service-viewer.component.html',
  styleUrls: ['./service-viewer.component.scss']
})
export class ServiceViewerComponent implements OnInit {

  _metaDataSources: Observable<MetaDataSourceDto[]>;

  constructor(private metaDataSourceService: MetaDataSource) { }

  ngOnInit() {
    this._metaDataSources = this.metaDataSourceService.getMetaDataSources();
  }
}
