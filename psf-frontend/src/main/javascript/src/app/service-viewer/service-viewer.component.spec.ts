import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceViewerComponent } from './service-viewer.component';

describe('ServiceViewerComponent', () => {
  let component: ServiceViewerComponent;
  let fixture: ComponentFixture<ServiceViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
