open module psf.frontend {
    requires lombok;

    requires spring.boot.autoconfigure;
    requires spring.boot;
    requires spring.context;
    requires spring.webmvc;
    requires spring.core;
    requires spring.beans;
    requires spring.web;
    requires com.fasterxml.classmate;
    requires org.apache.logging.log4j;
    requires psf.api;
    requires java.ws.rs;
    requires java.servlet;

    requires org.slf4j;
    requires resteasy.client;
}
