package io.ropi.psf.frontend.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Documented
public @interface BackendContextRoot {

    /**
     * Context root of the service the proxy calls.
     *
     * @return the context root
     */
    @AliasFor("contextRoot")
    String value() default JaxRsConfig.DEFAULT_CONTEXT_ROOT;

    /**
     * Context root of the service the proxy calls.
     *
     * @return the context root
     */
    @AliasFor("value")
    String contextRoot() default JaxRsConfig.DEFAULT_CONTEXT_ROOT;
}
