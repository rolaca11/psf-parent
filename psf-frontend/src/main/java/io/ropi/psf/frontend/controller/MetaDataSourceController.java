package io.ropi.psf.frontend.controller;

import java.util.List;

import io.ropi.psf.api.metadatasource.MetaDataSourceDto;
import io.ropi.psf.api.metadatasource.MetaDataSourceResource;
import io.ropi.psf.frontend.config.BackendContextRoot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/metadatasource")
public class MetaDataSourceController {
    private final MetaDataSourceResource metaDataSourceResource;

    public MetaDataSourceController(@BackendContextRoot MetaDataSourceResource metaDataSourceResource) {
        this.metaDataSourceResource = metaDataSourceResource;
    }

    @GetMapping
    public List<MetaDataSourceDto> getMetaDataSources() {
        return metaDataSourceResource.getMetaDataSources();
    }
}
