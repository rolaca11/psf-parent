package io.ropi.psf.frontend.config;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

import io.ropi.psf.api.metadatasource.MetaDataSourceResource;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
public class JaxRsConfig {
    static final String DEFAULT_CONTEXT_ROOT = "backend";

    /**
     * Puts the Host header in requests.
     *
     * @return clientRequestFilter
     */
    @Bean
    public ClientRequestFilter hostHeaderFilter() {
        return requestContext -> {
            requestContext.getHeaders().add("Host", "backend.ropi.io");
        };
    }

    /**
     * Rest client.
     *
     * @param clientRequestFilters clientRequestFilters
     * @return rest client
     */
    @Bean(destroyMethod = "close")
    public ResteasyClient restClient(List<ClientRequestFilter> clientRequestFilters) {
        ResteasyClient client = (ResteasyClient) ClientBuilder.newBuilder().build();
        for (ClientRequestFilter filter : clientRequestFilters) {
            client.register(filter);
        }
        return client;
    }

    /**
     * Backend host.
     *
     * @return backend host
     */
    @Bean
    public JndiObjectFactoryBean backendHost() {
        JndiObjectFactoryBean factoryBean = new JndiObjectFactoryBean();
        factoryBean.setJndiName("java:global/backendHost");
        return factoryBean;
    }

    /**
     * MetaDataSourceResource generator.
     *
     * @param request request
     * @return proxy
     */
    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public MetaDataSourceResource metaDataSourceResource(HttpServletRequest request,
                                                         List<ClientRequestFilter> clientRequestFilters) {
        String contextRoot = DEFAULT_CONTEXT_ROOT;

        return restClient(clientRequestFilters).target(UriBuilder.fromPath(contextRoot)
                .scheme(request.getScheme())
                .host((String) backendHost().getObject())
                .port(80)
                .build())
                .proxyBuilder(MetaDataSourceResource.class)
                .defaultConsumes(MediaType.APPLICATION_JSON_TYPE)
                .defaultProduces(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
