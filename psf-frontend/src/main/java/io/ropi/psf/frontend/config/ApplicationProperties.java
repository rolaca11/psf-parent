package io.ropi.psf.frontend.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("io.ropi.psf.frontend")
public class ApplicationProperties {

    private RestProperties rest;

    @Data
    public static class RestProperties {
        private String contextRoot;
    }
}
