package io.ropi.psf.api.camera;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class CameraDto extends CameraNewDto {
    private Long id;
}
