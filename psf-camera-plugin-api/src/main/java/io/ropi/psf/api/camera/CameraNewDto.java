package io.ropi.psf.api.camera;

import io.ropi.psf.api.point.PointDto;

import lombok.Data;

@Data
public class CameraNewDto {
    private Long dataSourceId;
    private PointDto position;
}
