package io.ropi.psf.api.camera;

import lombok.Data;

@Data
public class ImageDto {
    private byte[] bytes;
}
