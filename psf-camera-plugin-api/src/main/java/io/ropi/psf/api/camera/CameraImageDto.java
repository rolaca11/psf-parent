package io.ropi.psf.api.camera;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CameraImageDto extends CameraDetailsDto {
    private byte[] image;
}
