package io.ropi.psf.api.camera;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Path("/camera")
public interface CameraResource {

    @GET
    List<CameraDto> getCameras();

    @GET
    @Path("/{id}")
    CameraDetailsDto getCamera(@PathParam("id") long id);

    @POST
    CameraDto addCamera(CameraNewDto camera);

    @POST
    void sendImage(CameraImageDto image);

    @PUT
    CameraDetailsDto replaceCamera(CameraDto camera);

    @PATCH
    CameraDetailsDto editCamera(CameraDto camera);

    @DELETE
    @Path("/{id}")
    CameraDetailsDto deleteCamera(@PathParam("id") long id);
}
