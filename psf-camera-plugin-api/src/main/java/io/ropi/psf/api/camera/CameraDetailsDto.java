package io.ropi.psf.api.camera;

import io.ropi.psf.api.point.PointDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CameraDetailsDto extends CameraDto {
    private float height;
    private float pitch;
    private float yaw;
}
