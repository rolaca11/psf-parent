open module psf.camera.api {
    exports io.ropi.psf.api.camera;

    requires transitive psf.api;
    requires jakarta.ejb.api;
    requires java.ws.rs;

    requires lombok;
}
