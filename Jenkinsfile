node {
    def mvnHome
    stage('Preparation') {
        checkout scm
        mvnHome = tool 'M3'
    }
    stage('Compile') {
        // Run the maven build
        withEnv(["MVN_HOME=$mvnHome"]) {
	        sh '"$MVN_HOME/bin/mvn" clean compile'
        }
    }
    stage('Test') {
        // Run the maven build
        withEnv(["MVN_HOME=$mvnHome"]) {
	        sh '"$MVN_HOME/bin/mvn" test'
        }
    }
    stage('Package') {
        // Run the maven build
        withEnv(["MVN_HOME=$mvnHome"]) {
	        sh '"$MVN_HOME/bin/mvn" package'
        }
    }
    stage('Checkstyle') {
        // Run the maven build
        withEnv(["MVN_HOME=$mvnHome"]) {
	        sh '"$MVN_HOME/bin/mvn" checkstyle:check'
        }
    }
    stage('Deploy') {
        // Run the maven build
        if(env.BRANCH_NAME == 'master') {
            try {
                withEnv(["MVN_HOME=$mvnHome"]) {
                    sh '"$MVN_HOME/bin/mvn" deploy'
                }
            } catch(error) {
                retry(2) {
                    withEnv(["MVN_HOME=$mvnHome"]) {
                        sh '"$MVN_HOME/bin/mvn" deploy'
                    }
                }
            }
        }
    }
    stage('SonarQube analysis') {
        withSonarQubeEnv('SonarCloud') { // You can override the credential to be used
            sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -Dsonar.organization=ropisoft -Dsonar.branch.name=' + env.BRANCH_NAME
        }
    }
    stage('Results') {
        junit '**/target/surefire-reports/TEST-*.xml'
    }
}
