package io.ropi.psf.backend.camera.plugin;

import javax.ejb.Remote;
import java.util.Collections;
import java.util.List;

import io.ropi.psf.api.parkingspace.ParkingSpaceDto;
import io.ropi.psf.api.plugin.DataSource;
import io.ropi.psf.backend.camera.entities.CameraState;

public class CameraDataSource implements DataSource {
    private final CameraState cameraState;

    public CameraDataSource(CameraState cameraState) {
        this.cameraState = cameraState;
    }

    @Override
    public List<ParkingSpaceDto> getParkingSpaces(Long parkingLotId) {
        ParkingSpaceDto parkingSpaceDto = new ParkingSpaceDto();
        parkingSpaceDto.setFrom(0.4f);
        parkingSpaceDto.setTo(0.5f);
        parkingSpaceDto.setParkingLotId(parkingLotId);

        return Collections.singletonList(parkingSpaceDto);
    }
}
