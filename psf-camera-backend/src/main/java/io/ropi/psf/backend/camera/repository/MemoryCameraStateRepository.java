package io.ropi.psf.backend.camera.repository;

import javax.ejb.Stateful;
import javax.inject.Inject;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import io.ropi.psf.backend.camera.entities.CameraState;

@Stateful
public class MemoryCameraStateRepository implements CameraStateRepository {
    private static final AtomicLong idCounter = new AtomicLong(0L);

    private final Map<Long, CameraState> state = new ConcurrentHashMap<>();

    @Inject
    public MemoryCameraStateRepository() {
    }

    @Override
    public <S extends CameraState> S save(S entity) {
        long id = idCounter.incrementAndGet();
        entity.setId(id);

        return entity;
    }

    @Override
    public <S extends CameraState> Iterable<S> saveAll(Iterable<S> entities) {
        return StreamSupport.stream(entities.spliterator(), false).map(this::save).collect(Collectors.toList());
    }

    @Override
    public Optional<CameraState> findById(Long id) {
        return Optional.ofNullable(state.get(id));
    }

    @Override
    public boolean existsById(Long id) {
        return state.containsKey(id);
    }

    @Override
    public Iterable<CameraState> findAll() {
        return state.values();
    }

    @Override
    public Iterable<CameraState> findAllById(Iterable<Long> ids) {
        return StreamSupport.stream(ids.spliterator(), false)
                .map(this::findById)
                .map(other -> other.orElse(null))
                .collect(Collectors.toList());
    }

    @Override
    public long count() {
        return state.size();
    }

    @Override
    public void deleteById(Long id) {
        state.remove(id);
    }

    @Override
    public void delete(CameraState entity) {
        state.remove(entity.getId());
    }

    @Override
    public void deleteAll(Iterable<? extends CameraState> entities) {
        entities.forEach(this::delete);
    }

    @Override
    public void deleteAll() {
        state.clear();
    }
}
