package io.ropi.psf.backend.camera.entities;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CameraState {
    private Long id;

    private float height;
    private float yaw;
    private float pitch;

    private byte[] image;

    private LocalDateTime lastUpdate;
}
