package io.ropi.psf.backend.camera.entities;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(indexes = {
        @Index(columnList = "dataSourceId", unique = true)
})
@Data
public class CameraEntity {

    @Id
    @GeneratedValue
    private Long id;

    private Long dataSourceId;

    @Embedded
    private PointEntity position;
}
