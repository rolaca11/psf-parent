package io.ropi.psf.backend.camera.entities;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class PointEntity {
    private double latitude;
    private double longitude;
}
