package io.ropi.psf.backend.camera.repository;

import io.ropi.psf.backend.camera.entities.CameraState;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CameraStateRepository extends CrudRepository<CameraState, Long> {
}
