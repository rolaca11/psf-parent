package io.ropi.psf.backend.camera.resources;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import io.ropi.psf.api.camera.CameraDetailsDto;
import io.ropi.psf.api.camera.CameraDto;
import io.ropi.psf.api.camera.CameraImageDto;
import io.ropi.psf.api.camera.CameraNewDto;
import io.ropi.psf.api.camera.CameraResource;
import io.ropi.psf.backend.camera.entities.CameraState;
import io.ropi.psf.backend.camera.mappers.CameraMapper;
import io.ropi.psf.backend.camera.repository.CameraRepository;
import io.ropi.psf.backend.camera.repository.CameraStateRepository;

@Path("/camera")
public class CameraResourceImpl implements CameraResource {

    private final CameraRepository cameraRepository;
    private final CameraStateRepository cameraStateRepository;
    private final CameraMapper cameraMapper;
    private final Clock clock;

    /**
     * Constructor.
     *  @param cameraRepository cameraRepository
     * @param cameraStateRepository cameraStateRepository
     * @param cameraMapper cameraMapper
     * @param clock clock
     */
    @Inject
    public CameraResourceImpl(CameraRepository cameraRepository,
                              CameraStateRepository cameraStateRepository,
                              CameraMapper cameraMapper,
                              Clock clock) {
        this.cameraRepository = cameraRepository;
        this.cameraStateRepository = cameraStateRepository;
        this.cameraMapper = cameraMapper;
        this.clock = clock;
    }

    public CameraResourceImpl() {
        this(null, null, null, null);
    }

    @Override
    @Transactional
    public List<CameraDto> getCameras() {
        return cameraMapper.map(cameraRepository.findAll());
    }

    @Override
    @Transactional
    public CameraDetailsDto getCamera(long id) {
        CameraState cameraState = cameraStateRepository.findById(id).orElseThrow(NotFoundException::new);

        return cameraRepository.findById(id)
                .map(entity -> cameraMapper.map(entity, cameraState))
                .orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public CameraDto addCamera(CameraNewDto camera) {
        return Optional.ofNullable(camera)
                .map(cameraMapper::map)
                .map(cameraRepository::save)
                .map(cameraMapper::map)
                .orElseThrow(BadRequestException::new);
    }

    @Override
    @Transactional
    public void sendImage(CameraImageDto image) {
        Optional.ofNullable(image)
                .map(CameraDto::getId)
                .flatMap(cameraStateRepository::findById)
                .map(entity -> cameraMapper.map(image, LocalDateTime.now(clock)))
                .orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public CameraDetailsDto replaceCamera(CameraDto camera) {
        CameraState cameraState = Optional.ofNullable(camera)
                .map(CameraDto::getId)
                .flatMap(cameraStateRepository::findById)
                .orElseThrow(NotFoundException::new);

        return Optional.of(camera)
                .map(CameraDto::getId)
                .flatMap(cameraRepository::findById)
                .map(entity -> cameraMapper.replace(camera, entity))
                .map(cameraRepository::save)
                .map(entity -> cameraMapper.map(entity, cameraState))
                .orElseThrow(BadRequestException::new);
    }

    @Override
    @Transactional
    public CameraDetailsDto editCamera(CameraDto camera) {
        CameraState cameraState = Optional.ofNullable(camera)
                .map(CameraDto::getId)
                .flatMap(cameraStateRepository::findById)
                .orElseThrow(NotFoundException::new);

        return Optional.of(camera)
                .map(CameraDto::getId)
                .flatMap(cameraRepository::findById)
                .map(entity -> cameraMapper.edit(camera, entity))
                .map(cameraRepository::save)
                .map(entity -> cameraMapper.map(entity, cameraState))
                .orElseThrow(BadRequestException::new);
    }

    @Override
    @Transactional
    public CameraDetailsDto deleteCamera(long id) {
        CameraState cameraState = Optional.of(id)
                .flatMap(cameraStateRepository::findById)
                .orElseThrow(NotFoundException::new);

        CameraDetailsDto result = Optional.of(id)
                .flatMap(cameraRepository::findById)
                .map(entity -> cameraMapper.map(entity, cameraState))
                .orElseThrow(NotFoundException::new);

        cameraRepository.deleteById(id);
        cameraStateRepository.deleteById(id);

        return result;
    }
}
