package io.ropi.psf.backend.camera.plugin;

import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;

import io.ropi.psf.api.plugin.DataSourceParams;
import io.ropi.psf.backend.camera.repository.CameraStateRepository;

import lombok.extern.slf4j.Slf4j;

@Stateless
@Slf4j
public class CameraDataSourceProducer {

    private final CameraStateRepository cameraRepository;

    @Inject
    public CameraDataSourceProducer(CameraStateRepository cameraRepository) {
        this.cameraRepository = cameraRepository;
    }

    public CameraDataSourceProducer() {
        this(null);
    }

    /**
     * Produces {@link CameraDataSource}.
     *
     * @param injectionPoint injectionPoint
     * @return new {@link CameraDataSource} instance
     */
    @Produces
    @DataSourceParams
    public CameraDataSource cameraDataSource(InjectionPoint injectionPoint) {
        DataSourceParams params = injectionPoint.getAnnotated().getAnnotation(DataSourceParams.class);
        if (params == null) {
            throw new IllegalArgumentException("@DataSourceParams does not appear on injection point.");
        } else if (!cameraRepository.existsById(params.id())) {
            throw new IllegalArgumentException("DataSource ID is invalid");
        }

        return new CameraDataSource(cameraRepository.findById(params.id()).orElseThrow(IllegalArgumentException::new));
    }
}
