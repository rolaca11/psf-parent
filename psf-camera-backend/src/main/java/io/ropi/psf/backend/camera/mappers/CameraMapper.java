package io.ropi.psf.backend.camera.mappers;

import java.time.LocalDateTime;
import java.util.List;

import io.ropi.psf.api.camera.CameraDetailsDto;
import io.ropi.psf.api.camera.CameraDto;
import io.ropi.psf.api.camera.CameraImageDto;
import io.ropi.psf.api.camera.CameraNewDto;
import io.ropi.psf.backend.camera.entities.CameraEntity;
import io.ropi.psf.backend.camera.entities.CameraState;
import io.ropi.psf.backend.camera.plugin.CameraDataSource;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(imports = CameraDataSource.class)
public interface CameraMapper {

    @Mapping(target = "yaw", source = "state.yaw")
    @Mapping(target = "position", source = "entity.position")
    @Mapping(target = "pitch", source = "state.pitch")
    @Mapping(target = "id", source = "entity.id")
    @Mapping(target = "height", source = "state.height")
    @Mapping(target = "dataSourceId", source = "entity.dataSourceId")
    CameraDetailsDto map(CameraEntity entity, CameraState state);

    List<CameraDto> map(Iterable<CameraEntity> entities);

    @Mapping(target = "id", ignore = true)
    CameraEntity map(CameraNewDto camera);

    CameraDto map(CameraEntity entity);

    @Mapping(target = "yaw", source = "image.yaw")
    @Mapping(target = "pitch", source = "image.pitch")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "height", source = "image.height")
    CameraState map(CameraImageDto image, LocalDateTime lastUpdate);

    @Mapping(target = "position",
            source = "position",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    CameraEntity replace(CameraDto camera, @MappingTarget CameraEntity entity);

    @Mapping(target = "position",
            source = "position",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    CameraEntity edit(CameraDto camera, @MappingTarget CameraEntity entity);
}
