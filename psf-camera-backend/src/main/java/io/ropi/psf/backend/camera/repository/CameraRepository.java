package io.ropi.psf.backend.camera.repository;

import io.ropi.psf.backend.camera.entities.CameraEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CameraRepository extends JpaRepository<CameraEntity, Long> {
}
