package io.ropi.psf.backend.camera.plugin;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.Topic;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;

import io.ropi.psf.api.metadatasource.MetaDataSourceDto;

import lombok.extern.slf4j.Slf4j;

@Singleton
@Slf4j
@Startup
public class CameraMetaDataSourceReporter {

    @Resource(lookup = "java:global/topic/ServiceDiscoveryTopic")
    private Topic serviceDiscoveryTopic;

    @Resource(lookup = "java:global/topic/ServicePingTopic")
    private Topic servicePingTopic;

    @Resource(lookup = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    private final LocalDateTime startTime;
    private final Clock clock;

    @Inject
    public CameraMetaDataSourceReporter(Clock clock) {
        this.startTime = LocalDateTime.now(clock);
        this.clock = clock;
    }

    public CameraMetaDataSourceReporter() {
        this(Clock.systemDefaultZone());
    }

    /**
     * Post construct.
     */
    @PostConstruct
    public void postConstruct() {
        Executors.newSingleThreadExecutor().execute(() -> {
            try (JMSContext context = connectionFactory.createContext()) {
                sendServiceData(context);
                while (true) {
                    receive(context);
                }
            }
        });
    }

    protected void receive(JMSContext context) {
        try (JMSConsumer consumer = context.createConsumer(servicePingTopic)) {
            consumer.receive();

            sendServiceData(context);
        }
    }

    protected void sendServiceData(JMSContext context) {
        MetaDataSourceDto body = new MetaDataSourceDto();
        body.setId("camera");
        body.setName("Camera Service");
        body.setUptime(Duration.between(startTime, LocalDateTime.now(clock)).toSeconds());

        context.createProducer().send(serviceDiscoveryTopic, body);
    }
}
