open module psf.camera.backend {
    requires psf.api;
    requires psf.camera.api;
    requires spring.data.jpa;
    requires spring.data.commons;
    requires java.persistence;
    requires lombok;
    requires jakarta.enterprise.cdi.api;
    requires jakarta.ejb.api;
    requires jakarta.inject.api;
    requires java.transaction;
    requires org.mapstruct;
    requires jakarta.jms.api;
    requires java.ws.rs;
    requires org.slf4j;

    requires java.sql;
    requires java.annotation;
    requires java.compiler;
}
