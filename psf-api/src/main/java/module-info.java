open module psf.api {
    exports io.ropi.psf.api.datasource;
    exports io.ropi.psf.api.parkinglot;
    exports io.ropi.psf.api.parkingspace;
    exports io.ropi.psf.api.metadatasource;
    exports io.ropi.psf.api.point;
    exports io.ropi.psf.api.plugin;

    requires java.ws.rs;
    requires org.slf4j;
    requires jakarta.ejb.api;

    requires lombok;
}
