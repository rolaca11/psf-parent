package io.ropi.psf.api.metadatasource;

import java.util.Collection;

public interface MetaDataSourceProvider {
    Collection<MetaDataSourceDto> metaDataSources();
}
