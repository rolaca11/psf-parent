package io.ropi.psf.api.parkinglot;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

import io.ropi.psf.api.point.PointDto;

/**
 * A Parking lot is defined as an area, where car-sized spaces shall be searched.
 */
@Path("/parkinglot")
public interface ParkingLotResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<ParkingLotDto> getAllParkingLots();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<ParkingLotDto> getAllParkingLotsInArea(
            @QueryParam("topLeftPoint") PointDto topLeftPoint,
            @QueryParam("bottomRightPoint") PointDto bottomRightPoint);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    ParkingLotDetailsDto getParkingLot(@PathParam("id") long id);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ParkingLotDto addParkingLot(ParkingLotDto parkingLot);

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ParkingLotDto replaceParkingLot(ParkingLotDto parkingLot);

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ParkingLotDto editParkingLot(ParkingLotDto parkingLot);

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    ParkingLotDto deleteParkingLot(@PathParam("id") long id);
}
