package io.ropi.psf.api.parkingspace;

import lombok.Data;

@Data
public class ParkingSpaceDto {
    private float from;
    private float to;

    private Long parkingLotId;
}
