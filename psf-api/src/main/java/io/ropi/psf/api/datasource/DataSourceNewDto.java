package io.ropi.psf.api.datasource;

import io.ropi.psf.api.plugin.DataSource;

import lombok.Data;

@Data
public class DataSourceNewDto {
    private String name;

    private Class<? extends DataSource> dataSourceClass;
}
