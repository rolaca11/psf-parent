package io.ropi.psf.api.plugin;

public @interface DataSourceParams {

    /**
     * ID of DataSource.
     *
     * @return ID of DataSource
     */
    long id() default -1L;
}
