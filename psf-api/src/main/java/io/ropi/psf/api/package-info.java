/**
 * API for the Parking Space Finder app.
 * Defines JAX-WS based interfaces and DTOs to be implemented by the backend and used by clients.
 */
package io.ropi.psf.api;
