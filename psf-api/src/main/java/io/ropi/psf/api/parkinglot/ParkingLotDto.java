package io.ropi.psf.api.parkinglot;

import java.util.List;

import io.ropi.psf.api.datasource.DataSourceDetailsDto;
import io.ropi.psf.api.point.PointDto;

import lombok.Builder;
import lombok.Data;

@Data
public class ParkingLotDto {
    private long id;
    private List<PointDto> points;

    private List<DataSourceDetailsDto> dataSources;
}
