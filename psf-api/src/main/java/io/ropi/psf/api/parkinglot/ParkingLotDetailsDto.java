package io.ropi.psf.api.parkinglot;

import java.util.ArrayList;
import java.util.List;

import io.ropi.psf.api.parkingspace.ParkingSpaceDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ParkingLotDetailsDto extends ParkingLotDto {
    private List<ParkingSpaceDto> parkingSpaces = new ArrayList<>();
}
