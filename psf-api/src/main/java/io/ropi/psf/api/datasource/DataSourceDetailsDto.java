package io.ropi.psf.api.datasource;

import java.util.List;

import io.ropi.psf.api.parkinglot.ParkingLotDto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class DataSourceDetailsDto extends DataSourceNewDto {
    private Long id;

    private List<ParkingLotDto> parkingLots;
}
