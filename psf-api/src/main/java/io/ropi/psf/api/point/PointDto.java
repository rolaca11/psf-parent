package io.ropi.psf.api.point;

import java.text.ParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class PointDto {
    static final Pattern QUERY_PATTERN = Pattern
            .compile("^([NnSs])=(-?[0-9]+(?:\\.[0-9]+)?),([EeWw])=(-?[0-9]+(?:\\.[0-9]+)?)$", Pattern.CASE_INSENSITIVE);
    private static final String QUERY_FORMAT = "n=%.7f,e=%.7f";

    /**
     * Northern latitude.
     */
    private double latitude;

    /**
     * Eastern longitude.
     */
    private double longitude;

    /**
     * Parse string with GPS coordinates.
     *
     * @param string GPS coordinates in the format: [N|S]=&lt;latitude&gt;,[E|W]=&lt;longitude&gt;
     * @return {@link PointDto} with the parsed information
     * @throws ParseException If the input string does not match the above format
     */
    public static PointDto fromString(String string) throws ParseException {
        log.trace("Converting query string: {} to PointDto", string);
        Matcher matcher = QUERY_PATTERN.matcher(string);
        if (matcher.matches()) {
            double latitude = Double.parseDouble(matcher.group(2));
            double longitude = Double.parseDouble(matcher.group(4));

            if (matcher.group(1).equalsIgnoreCase("s")) {
                latitude *= -1;
            }

            if (matcher.group(3).equalsIgnoreCase("w")) {
                longitude *= -1;
            }

            log.trace("Parsed Coordinates: N:{}, E:{}", latitude, longitude);

            return new PointDto(latitude, longitude);
        } else {
            throw new ParseException(string, 0);
        }
    }

    public boolean isIn(PointDto northWest, PointDto southEast) {
        return isSouthEastFrom(northWest) && isNorthWestFrom(southEast);
    }

    public boolean isSouthEastFrom(PointDto reference) {
        return reference.getLatitude() >= latitude
                && reference.getLongitude() <= longitude;
    }

    public boolean isNorthWestFrom(PointDto reference) {
        return reference.getLatitude() <= latitude
                && reference.getLongitude() >= longitude;
    }

    /**
     * Returns a string representation of the given {@link PointDto} instance that can be used as an URL query
     * parameter.
     *
     * @return A string with format: n=&lt;latitude&gt;,s=&lt;longitude&gt;
     */
    public String toQueryString() {
        String result = String.format(Locale.ENGLISH, QUERY_FORMAT, latitude, longitude);
        log.trace("Converting PointDto to query string: {}", result);

        return result;
    }
}
