package io.ropi.psf.api.metadatasource;

import java.io.Serializable;

import lombok.Data;

@Data
public class MetaDataSourceDto implements Serializable {
    private String id;
    private String name;
    private Long uptime;
}
