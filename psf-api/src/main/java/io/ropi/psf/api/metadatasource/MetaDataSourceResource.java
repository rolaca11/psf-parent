package io.ropi.psf.api.metadatasource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/metadatasource")
public interface MetaDataSourceResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<MetaDataSourceDto> getMetaDataSources();
}
