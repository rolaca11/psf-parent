package io.ropi.psf.api.plugin;

import javax.ejb.Remote;
import java.util.List;

import io.ropi.psf.api.parkingspace.ParkingSpaceDto;

/**
 * This interface provides a generic way to acquire the current state of different data sources.
 * Communication with data sources can be implemented in various ways, like through message-driven EJBs, or a REST
 * interface.
 */
@Remote
public interface DataSource {

    List<ParkingSpaceDto> getParkingSpaces(Long parkingLotId);
}
