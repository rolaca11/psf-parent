package io.ropi.psf.api.point;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.util.regex.Matcher;

import org.junit.jupiter.api.Test;

class PointDtoTest {

    @Test
    void fromStringPositivePositive() throws ParseException {
        String queryString = "n=15.6780000,e=10.0000000";
        PointDto subject = new PointDto(15.678d, 10.0d);

        assertThat(PointDto.fromString(queryString)).isEqualTo(subject);
    }

    @Test
    void fromStringSouthernWestern() throws ParseException {
        String queryString = "s=15.6780000,w=10.0000000";
        PointDto subject = new PointDto(-15.678d, -10.0d);

        assertThat(PointDto.fromString(queryString)).isEqualTo(subject);
    }

    @Test
    void fromStringNegativePositive() throws ParseException {
        String queryString = "n=-15.6780000,e=10.0000000";
        PointDto subject = new PointDto(-15.678d, 10.0d);

        assertThat(PointDto.fromString(queryString)).isEqualTo(subject);
    }

    @Test
    void fromStringPositiveNegative() throws ParseException {
        String queryString = "n=15.6780000,e=-10.0000000";
        PointDto subject = new PointDto(15.678d, -10.0d);

        assertThat(PointDto.fromString(queryString)).isEqualTo(subject);
    }

    @Test
    void fromStringNegativeNegative() throws ParseException {
        String queryString = "n=-15.6780000,e=-10.0000000";
        PointDto subject = new PointDto(-15.678d, -10.0d);

        assertThat(PointDto.fromString(queryString)).isEqualTo(subject);
    }

    @Test
    void toQueryString() {
        PointDto subject = new PointDto(15.678d, 10.0d);

        Matcher matcher = PointDto.QUERY_PATTERN.matcher(subject.toQueryString());
        assertThat(matcher.matches()).isTrue();
    }

    @Test
    void circularConversionTest() throws ParseException {
        PointDto subject = new PointDto(15.678d, 10.0d);

        assertThat(PointDto.fromString(subject.toQueryString())).isEqualTo(subject);
    }

    @Test
    void isSouthEastTest() {
        PointDto subject = new PointDto(10.0d, 10.0d);
        PointDto reference = new PointDto(11.0d, 9.0d);
        PointDto referenceFalse = new PointDto(9.0d, 11.0d);
        PointDto referenceFalse2 = new PointDto(9.0d, 9.0d);
        PointDto referenceFalse3 = new PointDto(9.0d, 11.0d);

        assertThat(subject.isSouthEastFrom(reference)).isTrue();
        assertThat(subject.isSouthEastFrom(referenceFalse)).isFalse();
        assertThat(subject.isSouthEastFrom(referenceFalse2)).isFalse();
        assertThat(subject.isSouthEastFrom(referenceFalse3)).isFalse();
    }

    @Test
    void isNorthWestTest() {
        PointDto subject = new PointDto(10.0d, 10.0d);
        PointDto reference = new PointDto(9.0d, 11.0d);
        PointDto referenceFalse = new PointDto(11.0d, 9.0d);
        PointDto referenceFalse2 = new PointDto(9.0d, 9.0d);
        PointDto referenceFalse3 = new PointDto(11.0d, 11.0d);

        assertThat(subject.isNorthWestFrom(reference)).isTrue();
        assertThat(subject.isNorthWestFrom(referenceFalse)).isFalse();
        assertThat(subject.isNorthWestFrom(referenceFalse2)).isFalse();
        assertThat(subject.isNorthWestFrom(referenceFalse3)).isFalse();
    }

    @Test
    void isInTest() {
        PointDto in = new PointDto(10.0d, 10.0d);
        PointDto out = new PointDto(12.0d, 10.0d);
        PointDto out2 = new PointDto(10.0d, 12.0d);
        PointDto out3 = new PointDto(8.0d, 10.0d);
        PointDto out4 = new PointDto(10.0d, 8.0d);

        PointDto referenceNW = new PointDto(11.0d, 9.0d);
        PointDto referenceSE = new PointDto(9.0d, 11.0d);

        assertThat(in.isIn(referenceNW, referenceSE)).isTrue();
        assertThat(out.isIn(referenceNW, referenceSE)).isFalse();
        assertThat(out2.isIn(referenceNW, referenceSE)).isFalse();
        assertThat(out3.isIn(referenceNW, referenceSE)).isFalse();
        assertThat(out4.isIn(referenceNW, referenceSE)).isFalse();
    }
}
