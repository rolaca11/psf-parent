open module psf.backend {
    requires psf.api;
    requires spring.data.jpa;
    requires java.persistence;
    requires lombok;
    requires jakarta.enterprise.cdi.api;
    requires jakarta.inject.api;
    requires java.transaction;
    requires org.mapstruct;
    requires jakarta.ejb.api;
    requires jakarta.jms.api;
    requires java.ws.rs;
    requires org.slf4j;

    requires java.sql;
    requires java.annotation;
    requires java.compiler;
}
