package io.ropi.psf.backend;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class JaxrsApplication extends Application {
}
