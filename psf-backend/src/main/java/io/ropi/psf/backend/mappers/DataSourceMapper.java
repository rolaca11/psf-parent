package io.ropi.psf.backend.mappers;

import io.ropi.psf.api.datasource.DataSourceDetailsDto;
import io.ropi.psf.api.datasource.DataSourceNewDto;
import io.ropi.psf.backend.entities.DataSourceEntity;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface DataSourceMapper {

    DataSourceDetailsDto map(DataSourceEntity entity);

    @Mapping(target = "id", ignore = true)
    DataSourceEntity map(DataSourceNewDto dto);

    @Mapping(target = "id", ignore = true)
    DataSourceEntity map(DataSourceDetailsDto dto);

    @Mapping(source = "dataSourceClass",
            target = "dataSourceClass",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "name", target = "name",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    DataSourceEntity edit(DataSourceDetailsDto dto, @MappingTarget DataSourceEntity entity);

    @Mapping(source = "dataSourceClass",
            target = "dataSourceClass",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    @Mapping(source = "name",
            target = "name",
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    DataSourceEntity replace(DataSourceDetailsDto dto, @MappingTarget DataSourceEntity entity);
}
