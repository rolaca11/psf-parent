package io.ropi.psf.backend.repositories;

import io.ropi.psf.backend.entities.ParkingLotEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkingLotRepository extends JpaRepository<ParkingLotEntity, Long> {
}
