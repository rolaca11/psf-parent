package io.ropi.psf.backend.resources;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;

import io.ropi.psf.api.parkinglot.ParkingLotDetailsDto;
import io.ropi.psf.api.parkinglot.ParkingLotDto;
import io.ropi.psf.api.parkinglot.ParkingLotResource;
import io.ropi.psf.api.plugin.DataSource;
import io.ropi.psf.api.plugin.DataSourceParams;
import io.ropi.psf.api.point.PointDto;
import io.ropi.psf.backend.entities.DataSourceEntity;
import io.ropi.psf.backend.entities.ParkingLotEntity;
import io.ropi.psf.backend.mappers.ParkingLotMapper;
import io.ropi.psf.backend.repositories.ParkingLotRepository;

@Path("/parkinglot")
public class ParkingLotResourceImpl implements ParkingLotResource {

    private final ParkingLotRepository parkingLotRepository;
    private final ParkingLotMapper parkingLotMapper;
    private final Instance<DataSource> dataSources;

    /**
     * Constructor.
     * @param parkingLotRepository parkingLotRepository
     * @param parkingLotMapper parkingLotMapper
     * @param dataSources dataSources
     */
    @Inject
    public ParkingLotResourceImpl(ParkingLotRepository parkingLotRepository,
                                  ParkingLotMapper parkingLotMapper,
                                  Instance<DataSource> dataSources) {
        this.parkingLotRepository = parkingLotRepository;
        this.parkingLotMapper = parkingLotMapper;
        this.dataSources = dataSources;
    }

    public ParkingLotResourceImpl() {
        this(null, null, null);
    }

    @Override
    @Transactional
    public List<ParkingLotDto> getAllParkingLots() {
        return parkingLotRepository.findAll().stream().map(parkingLotMapper::map).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public List<ParkingLotDto> getAllParkingLotsInArea(PointDto topLeftPoint, PointDto bottomRightPoint) {
        return parkingLotRepository.findAll().stream()
                .map(parkingLotMapper::map)
                .filter(result -> result.getPoints().stream()
                        .anyMatch(point -> point.isIn(topLeftPoint, bottomRightPoint)))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ParkingLotDetailsDto getParkingLot(long id) {
        ParkingLotEntity parkingLotEntity = parkingLotRepository.findById(id)
                .orElseThrow(NotFoundException::new);

        ParkingLotDetailsDto result = new ParkingLotDetailsDto();

        for (DataSourceEntity dataSourceEntity : parkingLotEntity.getDataSources()) {
            DataSourceParams dataSourceParams = new DataSourceParams() {
                @Override
                public Class<? extends Annotation> annotationType() {
                    return DataSourceParams.class;
                }

                @Override
                public long id() {
                    return dataSourceEntity.getId();
                }
            };
            DataSource dataSource = dataSources.select(dataSourceEntity.getDataSourceClass(), dataSourceParams).get();
            result.getParkingSpaces().addAll(dataSource.getParkingSpaces(id));
        }

        ParkingLotDto parkingLot = parkingLotMapper.map(parkingLotEntity);
        result.setId(parkingLot.getId());
        result.setPoints(parkingLot.getPoints());

        return result;
    }

    @Override
    @Transactional
    public ParkingLotDto addParkingLot(ParkingLotDto parkingLot) {
        return parkingLotMapper.map(parkingLotRepository.save(parkingLotMapper.mapNoId(parkingLot)));
    }

    @Override
    @Transactional
    public ParkingLotDto replaceParkingLot(ParkingLotDto parkingLot) {
        return parkingLotRepository.findById(parkingLot.getId())
                .map(entity -> parkingLotMapper.replace(parkingLot, entity))
                .map(parkingLotRepository::save)
                .map(parkingLotMapper::map)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public ParkingLotDto editParkingLot(ParkingLotDto parkingLot) {
        return parkingLotRepository.findById(parkingLot.getId())
                .map(entity -> parkingLotMapper.edit(parkingLot, entity))
                .map(parkingLotRepository::save)
                .map(parkingLotMapper::map)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public ParkingLotDto deleteParkingLot(long id) {
        ParkingLotDto result = parkingLotRepository.findById(id)
                .map(parkingLotMapper::map)
                .orElseThrow(NotFoundException::new);

        parkingLotRepository.deleteById(id);

        return result;
    }
}
