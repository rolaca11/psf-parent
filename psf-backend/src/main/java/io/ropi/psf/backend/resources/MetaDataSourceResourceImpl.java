package io.ropi.psf.backend.resources;

import javax.inject.Inject;
import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.List;

import io.ropi.psf.api.metadatasource.MetaDataSourceDto;
import io.ropi.psf.api.metadatasource.MetaDataSourceProvider;
import io.ropi.psf.api.metadatasource.MetaDataSourceResource;

@Path("/metadatasource")
public class MetaDataSourceResourceImpl implements MetaDataSourceResource {

    private final MetaDataSourceProvider metaDataSourceProvider;

    public MetaDataSourceResourceImpl() {
        this(null);
    }

    @Inject
    public MetaDataSourceResourceImpl(MetaDataSourceProvider metaDataSourceProvider) {
        this.metaDataSourceProvider = metaDataSourceProvider;
    }

    @Override
    public List<MetaDataSourceDto> getMetaDataSources() {
        return new ArrayList<>(metaDataSourceProvider.metaDataSources());
    }
}
