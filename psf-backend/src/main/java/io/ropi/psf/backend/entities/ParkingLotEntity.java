package io.ropi.psf.backend.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ParkingLotEntity {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue
    private Long id;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<PointEntity> points;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @ToString.Exclude
    private List<DataSourceEntity> dataSources;
}
