package io.ropi.psf.backend.resources;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import io.ropi.psf.api.datasource.DataSourceDetailsDto;
import io.ropi.psf.api.datasource.DataSourceNewDto;
import io.ropi.psf.api.datasource.DataSourceResource;
import io.ropi.psf.backend.mappers.DataSourceMapper;
import io.ropi.psf.backend.repositories.DataSourceRepository;

@Path("/datasource")
public class DataSourceResourceImpl implements DataSourceResource {

    private final DataSourceRepository dataSourceRepository;
    private final DataSourceMapper dataSourceMapper;

    @Inject
    public DataSourceResourceImpl(DataSourceRepository dataSourceRepository, DataSourceMapper dataSourceMapper) {
        this.dataSourceRepository = dataSourceRepository;
        this.dataSourceMapper = dataSourceMapper;
    }

    /**
     * Empty constructor.
     */
    public DataSourceResourceImpl() {
        this(null, null);
    }

    @Override
    public List<DataSourceDetailsDto> getDataSources() {
        return dataSourceRepository.findAll().stream().map(dataSourceMapper::map).collect(Collectors.toList());
    }

    @Override
    public DataSourceDetailsDto getDataSource(long id) {
        return dataSourceRepository.findById(id).map(dataSourceMapper::map).orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public DataSourceDetailsDto addDataSource(DataSourceNewDto parkingLot) {
        return Optional.ofNullable(parkingLot)
                .map(dataSourceMapper::map)
                .map(dataSourceRepository::save)
                .map(dataSourceMapper::map)
                .orElseThrow(BadRequestException::new);
    }

    @Override
    @Transactional
    public DataSourceDetailsDto replaceDataSource(DataSourceDetailsDto parkingLot) {
        return Optional.ofNullable(parkingLot)
                .map(DataSourceDetailsDto::getId)
                .flatMap(dataSourceRepository::findById)
                .map(entity -> dataSourceMapper.replace(parkingLot, entity))
                .map(dataSourceRepository::save)
                .map(dataSourceMapper::map)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public DataSourceDetailsDto editDataSource(DataSourceDetailsDto parkingLot) {
        return Optional.ofNullable(parkingLot)
                .map(DataSourceDetailsDto::getId)
                .flatMap(dataSourceRepository::findById)
                .map(entity -> dataSourceMapper.edit(parkingLot, entity))
                .map(dataSourceRepository::save)
                .map(dataSourceMapper::map)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    @Transactional
    public DataSourceDetailsDto deleteDataSource(long id) {
        DataSourceDetailsDto result = dataSourceRepository
                .findById(id)
                .map(dataSourceMapper::map)
                .orElseThrow(NotFoundException::new);

        dataSourceRepository.deleteById(id);

        return result;
    }
}
