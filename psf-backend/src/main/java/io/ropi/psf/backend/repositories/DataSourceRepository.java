package io.ropi.psf.backend.repositories;

import io.ropi.psf.backend.entities.DataSourceEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DataSourceRepository extends JpaRepository<DataSourceEntity, Long> {
}
