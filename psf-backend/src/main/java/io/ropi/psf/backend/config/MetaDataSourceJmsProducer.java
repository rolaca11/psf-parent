package io.ropi.psf.backend.config;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.Topic;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import io.ropi.psf.api.metadatasource.MetaDataSourceDto;
import io.ropi.psf.api.metadatasource.MetaDataSourceProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
@Startup
public class MetaDataSourceJmsProducer implements MetaDataSourceProvider {

    @Resource(lookup = "java:global/topic/ServiceDiscoveryTopic")
    private Topic serviceDiscoveryTopic;

    @Resource(lookup = "java:global/topic/ServicePingTopic")
    private Topic servicePingTopic;

    @Resource(lookup = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    private final Map<String, MetaDataSourceDto> dataSources = new HashMap<>();

    /**
     * Post construct.
     */
    @PostConstruct
    public void postConstruct() {
        Executors.newSingleThreadExecutor().execute(() -> {
            try (JMSContext context = connectionFactory.createContext()) {
                while (true) {
                    receive(context);
                }
            }
        });
        Executors.newSingleThreadExecutor().execute(() -> {
            try (JMSContext context = connectionFactory.createContext()) {
                while (true) {
                    sendPing(context);
                    Thread.sleep(60000);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
    }

    /**
     * Receiver.
     */
    public void receive(JMSContext context) {
        try (JMSConsumer consumer = context.createConsumer(serviceDiscoveryTopic)) {
            MetaDataSourceDto metaDataSource = consumer.receiveBody(MetaDataSourceDto.class);
            log.debug("Message received: {}", metaDataSource.toString());

            dataSources.put(metaDataSource.getId(), metaDataSource);
        }
    }

    /**
     * Send ping.
     */
    public void sendPing(JMSContext context) {
        context.createProducer().send(servicePingTopic, (String) null);
    }

    @Override
    public Collection<MetaDataSourceDto> metaDataSources() {
        return dataSources.values();
    }
}
