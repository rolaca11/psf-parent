package io.ropi.psf.backend.config;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class EntityManagerProducer {

    @PersistenceContext
    private EntityManager entityManager;

    @Produces
    @RequestScoped
    public EntityManager entityManager() {
        return entityManager;
    }

    public void entityManager(@Disposes EntityManager entityManager) {
        entityManager.close();
    }
}
