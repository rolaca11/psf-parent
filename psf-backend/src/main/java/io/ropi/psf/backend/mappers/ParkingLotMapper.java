package io.ropi.psf.backend.mappers;

import io.ropi.psf.api.parkinglot.ParkingLotDto;
import io.ropi.psf.api.point.PointDto;
import io.ropi.psf.backend.entities.ParkingLotEntity;
import io.ropi.psf.backend.entities.PointEntity;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        uses = DataSourceMapper.class,
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ParkingLotMapper {

    ParkingLotDto map(ParkingLotEntity entity);

    /**
     * Maps an instance of {@link ParkingLotDto} to an instance of {@link ParkingLotEntity}.
     *
     * @param parkingLot dto to map
     * @return mapped entity
     */
    ParkingLotEntity map(ParkingLotDto parkingLot);

    PointDto map(PointEntity entity);

    @Mappings({
            @Mapping(target = "parkingLot", ignore = true),
            @Mapping(target = "id", ignore = true)
    })
    PointEntity map(PointDto point);

    /**
     * Updates {@link ParkingLotEntity} from the 2nd parameter with data from the 1st parameter. Null values of the
     * 1st parameter are ignored.
     *
     * @param parkingLot instance to use as data source
     * @param entity target entity
     * @return Updated entity
     */
    ParkingLotEntity edit(ParkingLotDto parkingLot, @MappingTarget ParkingLotEntity entity);

    /**
     * Updates {@link ParkingLotEntity} from the 2nd parameter with data from the 1st parameter. Null values of the
     * 1st parameter is used as well.
     *
     * @param parkingLot instance to use as data source
     * @param entity target entity
     * @return Updated entity
     */
    @Mappings({
            @Mapping(target = "points", source = "points",
                    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
    })
    ParkingLotEntity replace(ParkingLotDto parkingLot, @MappingTarget ParkingLotEntity entity);

    /**
     * Maps {@link ParkingLotDto} to {@link ParkingLotEntity} ignoring the ID of the former.
     *
     * @param parkingLot {@link ParkingLotDto}
     * @return {@link ParkingLotEntity}
     */
    @Mappings({
            @Mapping(target = "id", ignore = true)
    })
    ParkingLotEntity mapNoId(ParkingLotDto parkingLot);
}
