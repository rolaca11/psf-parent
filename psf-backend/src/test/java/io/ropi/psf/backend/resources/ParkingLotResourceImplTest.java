package io.ropi.psf.backend.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import javax.enterprise.inject.spi.CDIProvider;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import io.ropi.psf.api.parkinglot.ParkingLotDetailsDto;
import io.ropi.psf.api.parkinglot.ParkingLotDto;
import io.ropi.psf.api.parkingspace.ParkingSpaceDto;
import io.ropi.psf.api.plugin.DataSource;
import io.ropi.psf.api.point.PointDto;
import io.ropi.psf.backend.entities.DataSourceEntity;
import io.ropi.psf.backend.entities.ParkingLotEntity;
import io.ropi.psf.backend.entities.PointEntity;
import io.ropi.psf.backend.mappers.DataSourceMapper;
import io.ropi.psf.backend.mappers.DataSourceMapperImpl;
import io.ropi.psf.backend.mappers.ParkingLotMapper;
import io.ropi.psf.backend.mappers.ParkingLotMapperImpl;
import io.ropi.psf.backend.repositories.ParkingLotRepository;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParkingLotResourceImplTest {

    public static final long PARKING_LOT_ID = 10L;
    public static final double POINT1_LATITUDE = 10.0D;
    public static final double POINT1_LONGITUDE = 10.0D;

    public static final double POINT2_LATITUDE = 10.0D;
    public static final double POINT2_LONGITUDE = 10.0D;
    
    @Spy
    private DataSourceMapper dataSourceMapper = Mappers.getMapper(DataSourceMapper.class);
    
    @InjectMocks
    private ParkingLotMapper parkingLotMapper = Mappers.getMapper(ParkingLotMapper.class);

    private static DataSource dataSource = mock(DataSource.class);

    @BeforeAll
    static void setUpCdi() {
        CDIProvider cdiProvider = mock(CDIProvider.class);

        CDI<Object> cdi = mock(CDI.class);
        doReturn(cdi).when(cdiProvider).getCDI();

        Instance<DataSource> instance = mock(Instance.class);
        doReturn(instance).when(cdi).select(any(Class.class), any());

        Instance<DataSource> dataSources = mock(Instance.class);
        doReturn(dataSources).when(instance).select(eq(DataSource.class), any());

        doReturn(dataSource).when(dataSources).get();

        CDI.setCDIProvider(cdiProvider);
    }

    @Test
    void getAllParkingLots() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
        parkingLotEntity.setId(PARKING_LOT_ID);

        PointEntity point1 = new PointEntity();
        point1.setId(100L);
        point1.setLatitude(POINT1_LATITUDE);
        point1.setLongitude(POINT1_LONGITUDE);
        point1.setParkingLot(parkingLotEntity);

        PointEntity point2 = new PointEntity();
        point2.setId(200L);
        point2.setLatitude(POINT2_LATITUDE);
        point2.setLongitude(POINT2_LONGITUDE);
        point2.setParkingLot(parkingLotEntity);

        parkingLotEntity.setPoints(Arrays.asList(point1, point2));

        doReturn(Collections.singletonList(parkingLotEntity)).when(parkingLotRepository).findAll();

        ParkingLotDto expected = new ParkingLotDto();
        expected.setId(PARKING_LOT_ID);

        PointDto expectedPoint1 = new PointDto();
        expectedPoint1.setLatitude(POINT1_LATITUDE);
        expectedPoint1.setLongitude(POINT1_LONGITUDE);

        PointDto expectedPoint2 = new PointDto();
        expectedPoint2.setLatitude(POINT2_LATITUDE);
        expectedPoint2.setLongitude(POINT2_LONGITUDE);

        expected.setPoints(Arrays.asList(expectedPoint1, expectedPoint2));

        assertThat(subject.getAllParkingLots()).containsExactly(expected);
    }

    @Test
    void getAllParkingLotsInArea() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        ParkingLotEntity northOfBox = new ParkingLotEntity();
        northOfBox.setId(0L);
        northOfBox.setPoints(Arrays.asList(PointEntity.builder().latitude(12.0D).longitude(10.0D).build(), PointEntity.builder().latitude(13.0D).longitude(10.0D).build()));

        ParkingLotEntity fullyInBox = new ParkingLotEntity();
        fullyInBox.setId(1L);
        fullyInBox.setPoints(Arrays.asList(PointEntity.builder().latitude(10.5D).longitude(9.5D).build(), PointEntity.builder().latitude(9.5D).longitude(10.5D).build()));

        ParkingLotEntity partlyInBox = new ParkingLotEntity();
        partlyInBox.setId(2L);
        partlyInBox.setPoints(Arrays.asList(PointEntity.builder().latitude(11.5D).longitude(9.5D).build(), PointEntity.builder().latitude(9.5D).longitude(10.5D).build()));

        ParkingLotEntity southOfBox = new ParkingLotEntity();
        southOfBox.setId(3L);
        southOfBox.setPoints(Arrays.asList(PointEntity.builder().latitude(8.5D).longitude(9.5D).build(), PointEntity.builder().latitude(7.5D).longitude(10.5D).build()));

        doReturn(Arrays.asList(northOfBox, fullyInBox, partlyInBox, southOfBox)).when(parkingLotRepository).findAll();

        PointDto topLeftPoint = new PointDto(11.0D, 9.0D);
        PointDto bottomRightPoint = new PointDto(9.0D, 11.0D);
        assertThat(subject.getAllParkingLotsInArea(topLeftPoint, bottomRightPoint))
                .hasSize(2)
                .anySatisfy(dto -> assertThat(dto.getId()).isEqualTo(1L))
                .anySatisfy(dto -> assertThat(dto.getId()).isEqualTo(2L));
    }

    @Test
    void getParkingLot() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        ParkingLotEntity fullyInBox = new ParkingLotEntity();
        DataSourceEntity dataSourceEntity = new DataSourceEntity();
        dataSourceEntity.setId(256L);
        dataSourceEntity.setDataSourceClass(DataSource.class);

        fullyInBox.setId(1L);
        fullyInBox.setPoints(Arrays.asList(PointEntity.builder().latitude(10.5D).longitude(9.5D).build(), PointEntity.builder().latitude(9.5D).longitude(10.5D).build()));
        fullyInBox.setDataSources(Collections.singletonList(dataSourceEntity));

        doReturn(Optional.of(fullyInBox)).when(parkingLotRepository).findById(any());

        ParkingLotDetailsDto expected = new ParkingLotDetailsDto();
        expected.setId(1L);
        expected.setPoints(Arrays.asList(new PointDto(10.5D, 9.5D), new PointDto(9.5D, 10.5D)));

        assertThat(subject.getParkingLot(1L))
                .isEqualTo(expected);
    }

    @Test
    void getParkingLotNotFound() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        doReturn(Optional.empty()).when(parkingLotRepository).findById(any());

        assertThatThrownBy(() -> subject.getParkingLot(1L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void addParkingLot() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        doAnswer(invocation -> {
            ParkingLotEntity argument = invocation.getArgument(0);

            assertThat(argument.getId()).isNull();
            assertThat(argument.getPoints())
                    .hasSize(2)
                    .anySatisfy(entity -> assertThat(entity)
                            .satisfies(point -> assertThat(point.getLatitude()).isEqualTo(POINT1_LATITUDE))
                            .satisfies(point -> assertThat(point.getLongitude()).isEqualTo(POINT1_LONGITUDE)))
                    .anySatisfy(entity -> assertThat(entity)
                            .satisfies(point -> assertThat(point.getLatitude()).isEqualTo(POINT2_LATITUDE))
                            .satisfies(point -> assertThat(point.getLongitude()).isEqualTo(POINT2_LONGITUDE)));

            return argument;
        }).when(parkingLotRepository).save(any());

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(Arrays.asList(new PointDto(POINT1_LATITUDE, POINT1_LONGITUDE), new PointDto(POINT2_LATITUDE, POINT2_LONGITUDE)));

        subject.addParkingLot(parkingLot);

        verify(parkingLotRepository, times(1)).save(any());
    }

    @Test
    void replaceParkingLot() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
        parkingLotEntity.setId(PARKING_LOT_ID);
        parkingLotEntity.setPoints(new ArrayList<>(Collections.singletonList(
                PointEntity.builder().longitude(POINT1_LONGITUDE).latitude(POINT1_LATITUDE).build())));

        doReturn(Optional.of(parkingLotEntity)).when(parkingLotRepository).findById(anyLong());
        doAnswer(invocation -> {
            ParkingLotEntity argument = invocation.getArgument(0, ParkingLotEntity.class);

            assertThat(argument.getId()).isEqualTo(PARKING_LOT_ID);
            assertThat(argument.getPoints()).isNullOrEmpty();

            return argument;
        }).when(parkingLotRepository).save(any());

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(null);

        subject.replaceParkingLot(parkingLot);
    }

    @Test
    void replaceParkingLotNotFound() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        doReturn(Optional.empty()).when(parkingLotRepository).findById(anyLong());

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(Arrays.asList(new PointDto(POINT1_LATITUDE, POINT1_LONGITUDE), new PointDto(POINT2_LATITUDE, POINT2_LONGITUDE)));

        assertThatThrownBy(() -> subject.replaceParkingLot(parkingLot)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void editParkingLot() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
        parkingLotEntity.setId(PARKING_LOT_ID);
        parkingLotEntity.setPoints(new ArrayList<>(Collections.singletonList(
                PointEntity.builder().longitude(POINT1_LONGITUDE).latitude(POINT1_LATITUDE).build())));

        doReturn(Optional.of(parkingLotEntity)).when(parkingLotRepository).findById(anyLong());
        doAnswer(invocation -> {
            ParkingLotEntity argument = invocation.getArgument(0, ParkingLotEntity.class);

            assertThat(argument.getId()).isEqualTo(PARKING_LOT_ID);
            assertThat(argument.getPoints())
                    .hasSize(1)
                    .allSatisfy(point -> assertThat(point.getLongitude()).isEqualTo(POINT1_LONGITUDE))
                    .allSatisfy(point -> assertThat(point.getLatitude()).isEqualTo(POINT1_LATITUDE));

            return argument;
        }).when(parkingLotRepository).save(any());

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(null);

        subject.editParkingLot(parkingLot);
    }

    @Test
    void editParkingLotNotFound() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        doReturn(Optional.empty()).when(parkingLotRepository).findById(anyLong());

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(Arrays.asList(new PointDto(POINT1_LATITUDE, POINT1_LONGITUDE), new PointDto(POINT2_LATITUDE, POINT2_LONGITUDE)));

        assertThatThrownBy(() -> subject.editParkingLot(parkingLot)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void deleteParkingLot() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
        parkingLotEntity.setId(PARKING_LOT_ID);
        parkingLotEntity.setPoints(new ArrayList<>(Collections.singletonList(
                PointEntity.builder().longitude(POINT1_LONGITUDE).latitude(POINT1_LATITUDE).build())));

        doReturn(Optional.of(parkingLotEntity)).when(parkingLotRepository).findById(PARKING_LOT_ID);

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(Arrays.asList(new PointDto(POINT1_LATITUDE, POINT1_LONGITUDE), new PointDto(POINT2_LATITUDE, POINT2_LONGITUDE)));

        assertThatCode(() -> subject.deleteParkingLot(PARKING_LOT_ID)).doesNotThrowAnyException();
        verify(parkingLotRepository, times(1)).deleteById(PARKING_LOT_ID);
    }

    @Test
    void deleteParkingLotNotFound() {
        ParkingLotRepository parkingLotRepository = mock(ParkingLotRepository.class);
        ParkingLotResourceImpl subject = new ParkingLotResourceImpl(parkingLotRepository, parkingLotMapper, CDI.current().select(DataSource.class));

        doReturn(Optional.empty()).when(parkingLotRepository).findById(PARKING_LOT_ID);

        ParkingLotDto parkingLot = new ParkingLotDto();
        parkingLot.setId(PARKING_LOT_ID);
        parkingLot.setPoints(Arrays.asList(new PointDto(POINT1_LATITUDE, POINT1_LONGITUDE), new PointDto(POINT2_LATITUDE, POINT2_LONGITUDE)));

        assertThatThrownBy(() -> subject.deleteParkingLot(PARKING_LOT_ID)).isInstanceOf(NotFoundException.class);
        verify(parkingLotRepository, times(0)).deleteById(anyLong());
    }
}
